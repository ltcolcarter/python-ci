# Python CI Test Repository

**Note** - I created a very basic Python function along with some tests to confirm that the GitLab CI functionality of the project was working correctly. I only included this in my final submission just in case it was required and I didn't want to lose marks for plagarism or anything else.


- To run the GitLab CI pipeline you must upload this respository to a GitLab repository and then register a GitLab Runner on the system of your choice. 
- After this you can go to the CI/CD -> Pipelines tab on the left side and click on "Run Pipeline"

- Link: https://gitlab.com/ltcolcarter/python-ci/
