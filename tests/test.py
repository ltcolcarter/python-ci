import unittest
from src.app import squares
 
 
class Tests(unittest.TestCase):
    def basic_test(self):
        # Basic tests
        self.assertEqual(squares(13),169)
        self.assertNotEqual(squares(2),100)
 
    def invalid_test(self):
        # Check if it fails with non-numeric input
        with self.assertRaises(TypeError):
            squares("test")
 
 
def suite():
    _suite = unittest.TestSuite()
    _suite.addTest(Tests('basic_test'))
    _suite.addTest(Tests('invalid_test'))
    return _suite
